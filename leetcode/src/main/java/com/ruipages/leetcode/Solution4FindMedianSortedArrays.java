package com.ruipages.leetcode;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 18/12/24
 */
public class Solution4FindMedianSortedArrays {

    //TODO
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        return 0;
    }

    public static void main(String[] args) {
        Solution4FindMedianSortedArrays solution = new Solution4FindMedianSortedArrays();
        int [] n1 = new int[]{1,2,3,4};
        int [] n2 = new int[]{3,4,5,6};
        double reslut = solution.findMedianSortedArrays(n1, n2);
        System.out.println(reslut);
    }
}
