package com.ruipages.leetcode;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 19/1/2
 */
public class Solution7IntegerReverse {

    public int reverse(int x) {

        int n = Math.abs(x);

        int num = 0;
        while (n > 0){
            int i = n % 10;
            n = n / 10;
            int temp = num * 10 + i;
            if((temp - i)/10 != num){
                return 0;
            }
            num = temp;
        }

        return x > 0 ? num : -num;

    }

    public static void main(String[] args) {
        Solution7IntegerReverse solution =  new Solution7IntegerReverse();

        System.out.println(solution.reverse(-1230));

    }
}
