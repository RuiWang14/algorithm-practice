package com.ruipages.leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 19/1/2
 */
public class Solution6ZConvert {

    public String convert(String s, int numRows) {

        if (numRows < 2){
            return s;
        }

        char [] cc = s.toCharArray();
        List<List<Character>> store = new ArrayList<>();

        for (int i = 0; i < numRows; i++) {
            store.add(new ArrayList<>());
        }

        boolean flag = true;
        for (int i = 0; i < cc.length && flag; i += 2 * numRows - 2) {

            for (int j = 0; j < numRows; j++) {
                if (i + j < cc.length) {
                    store.get(j).add(cc[i + j]);
                }else {
                    flag = false;
                    break;
                }
            }

            for (int j = numRows, k = numRows - 2; k > 0 && flag; j++, k--) {
                if (i + j < cc.length){
                    store.get(k).add(cc[i + j]);
                }else {
                    flag = false;
                    break;
                }
            }
        }

//        store.forEach(list -> {
//            list.forEach(System.out::print);
//            System.out.println();
//        });

        StringBuilder sb = new StringBuilder(cc.length);
        store.forEach(list -> list.forEach(i -> sb.append(i)));

        return sb.toString();
    }

    public static void main(String[] args) {
        String s = "0123456789";
        Solution6ZConvert solution = new Solution6ZConvert();
        System.out.println(solution.convert(s,5));
    }
}
