package com.ruipages.leetcode;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 19/1/2
 */
public class ListNode {
    public int val;
    public ListNode next;

    ListNode(int x) {
        val = x;
    }

    public static void printNode(ListNode listNode){
        ListNode temp = listNode;
        while (temp != null){
            System.out.print(temp.val);
            temp = temp.next;
        }
    }

    public static ListNode buildNode(int [] vs){
        ListNode node = new ListNode(0);
        ListNode temp = node;
        for (int i: vs){
            temp.next = new ListNode(i);
            temp = temp.next;
        }

        return node.next;
    }
}

