package com.ruipages.leetcode;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 2019-01-08
 */

public class TreeNode implements AbstractTreeNode {
    public int val;
    public TreeNode left;
    public TreeNode right;

    TreeNode(int x) {
        val = x;
    }

    public static TreeNode buildTree(List<Integer> tree){

        if (tree.isEmpty()){
            return null;
        }
        Deque<TreeNode> deque = new ArrayDeque<>();
        TreeNode root = new TreeNode(tree.get(0));
        deque.offer(root);

        int j = 1;

        while (j < tree.size()){
            TreeNode t = deque.poll();

            if (tree.get(j) != null){
                t.left = new TreeNode(tree.get(j));
                deque.offer(t.left);
            }
            j += 1;

            if (j < tree.size() && tree.get(j) != null){
                t.right = new TreeNode(tree.get(j));
                deque.offer(t.right);
            }
            j += 1;
        }
        return root;
    }


    public static void main(String[] args) {
        TreeNode treeNode = buildTree(Arrays.asList(1,2,3,4,5,6,7,8,9,10,11,12,13));
        TreePrintUtil.pirnt(treeNode);
    }

    @Override
    public Object getVal() {
        return  val;
    }

    @Override
    public AbstractTreeNode getLeftChild() {
        return left;
    }

    @Override
    public AbstractTreeNode getRightChild() {
        return right;
    }

    @Override
    public String toString() {
        return "[" + val + "]";
    }
}

