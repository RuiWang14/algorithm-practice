package com.ruipages.leetcode;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 18/12/24
 */
public class Solution5LongestPalindrome2 {

    public String longestPalindrome(String s) {
        char [] cc = s.toCharArray();
        int f1 = 0, f2 = 0;

        for (int i = 0; i < cc.length; i++){
            for (int j = i; j < cc.length; j++){
                boolean flag = true;
                if (cc[i] == cc[j]){
                    int k=i,q=j;
                    while (k < q){
                        if (cc[k] == cc[q]){
                            k += 1;
                            q -= 1;
                        }else {
                            flag = false;
                            break;
                        }
                    }
                    if (flag && (f2 - f1 + 1 < j - i + 1)){
                        f1 = i;
                        f2 = j;
                    }

                }
            }
        }

        if (cc.length < f2 + 1) {
            return s.substring(f1, cc.length);
        }else {
            return s.substring(f1, f2 + 1);
        }
    }

    public static void main(String[] args) {
        String s= "abcba";
        Solution5LongestPalindrome2 solution = new Solution5LongestPalindrome2();
        System.out.println(solution.longestPalindrome(s));
    }
}
