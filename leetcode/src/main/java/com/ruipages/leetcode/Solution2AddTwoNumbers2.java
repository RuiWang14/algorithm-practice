package com.ruipages.leetcode;


/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 18/12/24
 */
public class Solution2AddTwoNumbers2 {

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode node = new ListNode(0);
        ListNode n1 = l1, n2 = l2, curr = node;
        int add = 0;
        while (n1 != null || n2 != null){
            int v1 = n1 == null ? 0 : n1.val;
            int v2 = n2 == null ? 0 : n2.val;

            int v = (v1 + v2 + add) % 10;
            add = (v1 + v2 + add) / 10;

            curr.next = new ListNode(v);
            curr = curr.next;

            n1 = n1 == null ? null : n1.next;
            n2 = n2 == null ? null : n2.next;
        }

        if (add > 0){
            curr.next = new ListNode(add);
        }

        return node.next;
    }

    public static void main(String[] args) {
        ListNode l1 = ListNode.buildNode(new int[] {1,2,3,4});
        ListNode l2 = ListNode.buildNode(new int[] {9,9,9,9});

        Solution2AddTwoNumbers2 solution = new Solution2AddTwoNumbers2();
        ListNode result = solution.addTwoNumbers(l1, l2);

        ListNode.printNode(result);
    }
}
