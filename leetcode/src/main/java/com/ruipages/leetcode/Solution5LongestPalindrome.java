package com.ruipages.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 18/12/24
 */
public class Solution5LongestPalindrome {

    public String longestPalindrome(String s) {
        char []  cc = s.toCharArray();

        Map<Character, List<Integer>> map = new HashMap<>();

        for (int i = 0; i < cc.length; i++){
            if (map.containsKey(cc[i])){
                map.get(cc[i]).add(i);
            }else {
                List<Integer> ll = new ArrayList<>();
                ll.add(i);
                map.put(cc[i], ll);
            }
        }

        int f1 = 0, f2 = 0;
        for (int i = 0; i < cc.length; i++){
            List<Integer> ll = map.get(cc[i]);
            for (int j : ll){
                boolean flag = true;
                int k = i;
                int q = j;
                while (k < q){
                    if (cc[k] == cc[q]){
                        k += 1;
                        q -= 1;
                    }else {
                        flag = false;
                        break;
                    }
                }
                if (flag && (f2 - f1 + 1 < j - i + 1)){
                        f1 = i;
                        f2 = j;
                }
            }
        }

        if (cc.length < f2 + 1) {
            return s.substring(f1, cc.length);
        }else {
            return s.substring(f1, f2 + 1);
        }

    }

    public static void main(String[] args) {
        String s= "aabaa";
        Solution5LongestPalindrome solution = new Solution5LongestPalindrome();
        System.out.println(solution.longestPalindrome(s));
    }

}
