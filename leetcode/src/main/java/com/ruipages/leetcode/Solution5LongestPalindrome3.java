package com.ruipages.leetcode;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 18/12/24
 */
public class Solution5LongestPalindrome3 {

    public String longestPalindrome(String s) {

        char [] cc = s.toCharArray();
        if(cc.length == 0){
            return s;
        }

        boolean [][] map = new boolean[cc.length][cc.length];

        map[0][0] = true;
        for (int i  = 1; i < cc.length; i ++){
            map[i][i] = true;
            map[i][i - 1] = true;
        }

        int resLeft = 0, resRight = 0;

        for (int step = 2; step <= cc.length; step++ ){
            for (int i = 0; i <= cc.length - step; i++ ){
                if (cc[i] == cc[i + step -1] && map[i+1][i + step - 2]){
                    map[i][i + step - 1] = true;
                    if (resRight - resLeft + 1 < step){
                        resLeft = i;
                        resRight = i + step - 1;
                    }
                }
            }
        }

        return s.substring(resLeft,resRight + 1);
    }

    public static void main(String[] args) {
        Solution5LongestPalindrome3 solution = new Solution5LongestPalindrome3();
        System.out.println(solution.longestPalindrome("aabaaa"));

    }

    public static void printMap(boolean[][] map){
        for (boolean[] ll : map){
            for (boolean i : ll){
                if (i){
                    System.out.print("T");
                }else {
                    System.out.print("F");
                }

                System.out.print(",");
            }
            System.out.println();
        }
    }
}
