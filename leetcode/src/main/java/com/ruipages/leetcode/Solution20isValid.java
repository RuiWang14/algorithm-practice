package com.ruipages.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 19/1/2
 */
public class Solution20isValid {

    private char getPartner(char c){
        if (c == '('){
            return ')';
        }else if (c == '{'){
            return '}';
        }else {
            return ']';
        }
    }

    public boolean isValid(String s) {

        char [] cc = s.toCharArray();
        Deque<Character> deque = new ArrayDeque<>();

        for (char c: cc){
            if (c == '(' || c == '[' || c == '{'){
                deque.push(c);
            }else {
                if (deque.isEmpty() || c != getPartner(deque.pop())){
                    return false;
                }
            }
        }

        return deque.isEmpty();
    }

    public static void main(String[] args) {
        Solution20isValid solution = new Solution20isValid();
        System.out.println(solution.isValid("(([]){})"));
    }
}
