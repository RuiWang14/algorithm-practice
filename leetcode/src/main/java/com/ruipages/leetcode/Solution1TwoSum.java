package com.ruipages.leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 18/12/24
 */
public class Solution1TwoSum {

    public int[] twoSum(int[] nums, int target) {

        Map<Integer, Integer> map = new HashMap<>(nums.length);

        for (int i = 0; i < nums.length; i++) {
            Integer com = target - nums[i];
            if (map.containsKey(com)){
                return new int[]{map.get(com), i};
            }
            map.put(nums[i], i);
        }

        return new int[]{-1,-1};
    }

    public static void main(String[] args) {
        Solution1TwoSum solution = new Solution1TwoSum();
        int [] nums = new int[]{3,2,4};

        int[] result = solution.twoSum(nums,6);
        System.out.println(Arrays.toString(result));
    }
}
