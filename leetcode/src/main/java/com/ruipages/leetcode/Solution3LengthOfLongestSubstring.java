package com.ruipages.leetcode;

import java.util.*;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 18/12/24
 */
public class Solution3LengthOfLongestSubstring {

    public int lengthOfLongestSubstring(String s) {
        char[] cc = s.toCharArray();
        Queue<Character> queue = new LinkedList<>();
        int max = 0;

        for (char c : cc){
            if (!queue.contains(c)){
                queue.offer(c);
            }else {
                while (queue.contains(c)){
                    queue.poll();
                }
                queue.offer(c);
            }
            max = Math.max(max, queue.size());
        }
        return max;
    }

    public static void main(String[] args) {
        Solution3LengthOfLongestSubstring solution = new Solution3LengthOfLongestSubstring();
        System.out.println(solution.lengthOfLongestSubstring("pwwkew"));

    }
}
