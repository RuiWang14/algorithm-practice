package com.ruipages.leetcode;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 18/12/27
 */
public class Solution5LongestPalindrome4 {

    public String longestPalindrome(String s) {

        char[] cc = s.toCharArray();

        if (cc.length == 0 || cc.length == 1){
            return s;
        }

        int left = 0, right = 0;

        for (int i = 0; i < cc.length; i++) {
            int j = 0;
            while (i - j >= 0 && i + j < cc.length && cc[i - j] == cc[i + j]) {
                if (right - left + 1 < 2 * j + 1) {
                    left = i - j;
                    right = i + j;
                }
                j += 1;
            }

            j = 0;
            while (i - j >= 0 && i + j + 1 < cc.length && cc[i - j] == cc[i + j + 1]) {
                if (right - left + 1 < 2 * j + 2) {
                    left = i - j;
                    right = i + j + 1;
                }
                j += 1;
            }
        }

        return s.substring(left, right + 1);
    }


    public static void main(String[] args) {
        Solution5LongestPalindrome4 solution = new Solution5LongestPalindrome4();
        System.out.println(solution.longestPalindrome("aaaba"));
        System.out.println(solution.longestPalindrome(""));
        System.out.println(solution.longestPalindrome("aaabaa"));
        System.out.println(solution.longestPalindrome("aaabba"));
        System.out.println(solution.longestPalindrome("abbd"));

    }

}
