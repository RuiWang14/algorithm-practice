package com.ruipages.leetcode;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 19/1/2
 */
public class Solution9isPalindrome {

    public boolean isPalindrome(int x) {
        StringBuilder sb = new StringBuilder(x + "");
        if (x < 0){
            sb.insert(0, "-");
        }

        return sb.toString().equals(sb.reverse().toString());
    }

    public static void main(String[] args) {
        Solution9isPalindrome solution = new Solution9isPalindrome();

        System.out.println(solution.isPalindrome(12321));
    }
}
