package com.ruipages.leetcode.solution27;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 2019-01-08
 */
public class RemoveElement {

    public int removeElement(int[] nums, int val) {

        int i = 0, j = 0;

        while (j < nums.length){
            if (nums[j] == val){
                j += 1;
            }else {
                nums[i] = nums[j];
                i += 1;
                j += 1;
            }
        }
        return i;
    }

    public static void main(String[] args) {
        RemoveElement solution = new RemoveElement();

        int [] nums = new int[] {};
        System.out.println(solution.removeElement(nums, 3));
        for (int n : nums) {
            System.out.print(n);
            System.out.print(",");
        }
    }
}
