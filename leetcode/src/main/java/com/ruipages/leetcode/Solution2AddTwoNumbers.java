package com.ruipages.leetcode;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 18/12/24
 */
public class Solution2AddTwoNumbers {

    public ListNode rec(ListNode result, ListNode l1, ListNode l2, int add){

        int v1 = 0, v2 = 0;
        ListNode n1 = null, n2 = null;
        if (l1 != null) {
            v1 = l1.val;
            n1 = l1.next;
        }
        if (l2 != null){
            v2 = l2.val;
            n2 = l2.next;
        }
        int v = v1 + v2 + add;

        if (n1 == null && n2 == null){
            ListNode t = new ListNode(v % 10 );
            if (v >= 10){
                t.next = new ListNode(v /10);
            }
            result.next = t;
        }else {
            result.next = rec(new ListNode(v % 10), n1, n2, v / 10);
        }

        return result;
    }

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {

        if (l1 == null && l2 == null){
            return null;
        }
        if (l1 == null){
            return l2;
        }
        if (l2 == null){
            return l1;
        }

        return rec(new ListNode(0),l1,l2,0).next;
    }

    public static void main(String[] args) {
        ListNode l1 = ListNode.buildNode(new int[] {1,2,3,4});
        ListNode l2 = ListNode.buildNode(new int[] {9,9,9,9});

        Solution2AddTwoNumbers solution = new Solution2AddTwoNumbers();
        ListNode result = solution.addTwoNumbers(l1, l2);

        ListNode.printNode(result);
    }
}

