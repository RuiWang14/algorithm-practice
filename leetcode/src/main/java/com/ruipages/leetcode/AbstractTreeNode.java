package com.ruipages.leetcode;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 2019-01-08
 */
public interface AbstractTreeNode {

    Object getVal();

    AbstractTreeNode getLeftChild();

    AbstractTreeNode getRightChild();
}
