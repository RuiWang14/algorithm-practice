package com.ruipages.leetcode.solution83;

import com.ruipages.leetcode.ListNode;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 2019-01-08
 */
public class DeleteDuplicates {

    public ListNode deleteDuplicates(ListNode head) {

        ListNode curr = head;

        while (curr != null && curr.next != null){
            if (curr.val == curr.next.val){
                curr.next = curr.next.next;
            }else {
                curr = curr.next;
            }
        }

        return head;
    }

    public static void main(String[] args) {
        DeleteDuplicates solution = new DeleteDuplicates();
        ListNode head = ListNode.buildNode(new int[] {});
        ListNode re = solution.deleteDuplicates(head);
        ListNode.printNode(re);
    }

}
