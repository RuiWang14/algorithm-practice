# 83. 删除排序链表中的重复元素

[题目链接](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-list/submissions/)

> 给定一个排序链表，删除所有重复的元素，使得每个元素只出现一次。

题目非常简单，因为是排序链表，只需要判断 curr == curr.next，如果相等，删掉
curr.net 就可以了。伪代码如下：

```
while curr != null && curr.next != null:
    if curr == curr.next:
        curr.next = curr.next.next
    else:
        curr = i.next

 //边界条件：
 curr != null && curr.next != null
 ```



