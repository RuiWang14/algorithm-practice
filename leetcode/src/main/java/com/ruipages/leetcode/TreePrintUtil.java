package com.ruipages.leetcode;

import java.util.*;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 2019-01-08
 */
public class TreePrintUtil {

    private static final int NULL_LENGTH = 4;

    public static void pirnt(AbstractTreeNode root) {

        List<List<AbstractTreeNode>> list = new ArrayList<>();
        list.add(Arrays.asList(root));

        int i = 0;
        while (true){
            List<AbstractTreeNode> nq = new ArrayList<>();
            for (AbstractTreeNode at : list.get(i)){
                if (at !=null){
                    nq.add(at.getLeftChild());
                    nq.add(at.getRightChild());
                }
            }
            if (!nq.isEmpty() && !isAllNull(nq)){
                list.add(nq);
                i += 1;
            }else {
                break;
            }
        }

        Map<AbstractTreeNode, Integer> position = buildPositionMap(list);

        for (int j = 0; j < list.size(); j++) {
            printLine(list.get(j), position);
        }
    }

    private static void printLine(List<AbstractTreeNode> list, Map<AbstractTreeNode, Integer> position){
        StringBuilder sb = new StringBuilder();
        StringBuilder si = new StringBuilder();

        for (AbstractTreeNode at : list){
            if (at != null) {
                int start = position.get(at);
                while (sb.length() < start) {
                    sb.append(" ");
                }
                sb.insert(start, at.toString());

                AbstractTreeNode lc = at.getLeftChild();
                if (lc != null) {
                    addCutOffLine(at,lc,"/",position,si);
                }

                AbstractTreeNode rc = at.getRightChild();
                if (rc != null) {
                    addCutOffLine(at,rc,"\\",position,si);
                }
            }

        }
        System.out.println(sb);
        System.out.println(si);


    }

    private static void addCutOffLine(AbstractTreeNode parent, AbstractTreeNode child, String cutOff, Map<AbstractTreeNode, Integer> position, StringBuilder sb){
        int ir = position.get(child);
        int start = position.get(parent);
        while (sb.length() < (start + ir + (child.toString().length() / 2 + child.toString().length() / 2)) / 2) {
            sb.append(" ");
        }
        sb.append(cutOff);
    }

    private static Map<AbstractTreeNode, Integer> buildPositionMap(List<List<AbstractTreeNode>> list){
        Map<AbstractTreeNode, Integer> map = new HashMap<>();

        List<AbstractTreeNode> lastLine = list.get(list.size() - 1);
        map.put(lastLine.get(0),0);
        int index = 0;
        for (AbstractTreeNode node : lastLine) {
            if (node != null) {
                map.put(node, index);
                index = index + node.toString().length() + 1;
            } else {
                index = index + NULL_LENGTH;
            }
        }

        for (int i = list.size() - 2 ; i >= 0 ; i--) {
            int lastIndex = 0;
            List<AbstractTreeNode> ll = list.get(i);
            for (AbstractTreeNode at : ll){
                if(at != null) {
                    AbstractTreeNode lc = at.getLeftChild();
                    AbstractTreeNode rc = at.getRightChild();
                    if (lc != null && rc != null) {
                        lastIndex = (map.get(lc) + map.get(rc)) / 2;
                        map.put(at, lastIndex);
                    } else if (lc != null) {
                        lastIndex = map.get(lc) + 2;
                        map.put(at, lastIndex);
                    } else if (rc != null) {
                        lastIndex = map.get(rc) + 2 - at.toString().length();
                        map.put(at, lastIndex);
                    } else {
                        lastIndex += 4;
                        map.put(at, lastIndex + NULL_LENGTH);
                    }
                }
            }
        }

        return map;
    }

    private static boolean isAllNull(List<AbstractTreeNode> list){
        for (AbstractTreeNode at : list){
            if (at != null){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        TreeNode treeNode = TreeNode.buildTree(Arrays.asList(1,2,3,4,5,6,7,8,9,null,null,null,null,12,13));
        TreePrintUtil.pirnt(treeNode);
    }

}
