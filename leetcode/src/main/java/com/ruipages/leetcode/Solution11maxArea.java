package com.ruipages.leetcode;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 19/1/2
 */
public class Solution11maxArea {

    public int maxArea(int[] height) {
        int i = 0, j = height.length -1;
        int max = 0;
        while (i < j){

            int h = Math.min(height[i],height[j]);
            max = Math.max(max, h * (j -i));

            if (height[i] < height[j]){
                i += 1;
            }else {
                j -= 1;
            }
        }
        return max;
    }

    public static void main(String[] args) {
        Solution11maxArea solution = new Solution11maxArea();
        System.out.println(solution.maxArea(new int [] {1,8,6,2,5,4,8,3,7}));
    }
}
