package com.ruipages.leetcode;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 19/1/2
 */
public class Solution19removeNthFromEnd {

    public ListNode removeNthFromEnd(ListNode head, int n) {

        ListNode now = new ListNode(0);
        now.next = head;

        int last = 0;
        ListNode curr = now;
        ListNode select = now;

        while (curr.next != null){
            last += 1;
            curr = curr.next;
            if (last >= n + 1){
                select = select.next;
            }
        }

        if (select.next != null){
            select.next = select.next.next;
        }else {
            select = null;
        }

        return now.next;
    }

    public static void main(String[] args) {
        Solution19removeNthFromEnd solution = new Solution19removeNthFromEnd();

        ListNode head = solution.removeNthFromEnd(ListNode.buildNode(new int[]{1,2,3,4,5}), 1);
        ListNode.printNode(head);
        System.out.println();

    }
}
