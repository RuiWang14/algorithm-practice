package com.ruipages.leetcode.solution26;

/**
 * Copyright (C) 1998 - 2018 SOHU Inc., All Rights Reserved.
 * <p>
 *
 * @author: Rui
 * @date: 2019-01-07
 */
public class RemoveDuplicates {

    public int removeDuplicates(int[] nums) {
        int i = 0, j = 0;

        if (nums.length < 2){
            return nums.length;
        }

        while (j < nums.length - 1) {
            j += 1;
            if (nums[i] != nums[j]){
                i += 1;
                nums[i] = nums[j];
            }
        }

        return i + 1;
    }

    public static void main(String[] args) {
        RemoveDuplicates solution = new RemoveDuplicates();
        System.out.println(solution.removeDuplicates(new int [] {1,1,1,1,1,1,1,1,2}));
    }
}
